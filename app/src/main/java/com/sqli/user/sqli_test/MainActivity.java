package com.sqli.user.sqli_test;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final static double MAX_VALUE = Math.pow(10,20);

    private Button start_button;
    private TextView resultTextView;
    private EditText inputText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start_button = (Button) findViewById(R.id.button);
        resultTextView = (TextView) findViewById(R.id.result_text);
        inputText = (EditText) findViewById(R.id.editText);

        start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!inputText.getText().equals("")||inputText.getText()==null){
                    if(Double.parseDouble(inputText.getText().toString())>MAX_VALUE){
                        Toast.makeText(MainActivity.this.getApplicationContext(),"Max allowed is 10^20",Toast.LENGTH_LONG).show();
                        return;
                    }
                    new FindPrimeNumbersAsyncTask(MainActivity.this).
                            execute(Double.parseDouble(inputText.getText().toString()));
                }
            }
        });

    }


    public void showResult(List<Integer> results){
        if(results==null||results.size()<=1){
            resultTextView.setText(getResources().getString(R.string.not_found_txt));
        }else{
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(getResources().getString(R.string.result_text)+"\r\n");
            for (Integer integer: results) {
                stringBuilder.append(integer+" x ");
            }

            stringBuilder.replace(stringBuilder.length()-3,stringBuilder.length(),"");
            resultTextView.setText(stringBuilder.toString());
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /* Save UI state changes to the savedInstanceState.
         This bundle will be passed to onCreate if the process is
         killed and restarted.*/
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        /* Restore UI state from the savedInstanceState.
         This bundle has also been passed to onCreate.*/
    }
}
