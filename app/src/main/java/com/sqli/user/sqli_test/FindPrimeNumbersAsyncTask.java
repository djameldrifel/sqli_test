package com.sqli.user.sqli_test;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 20/04/2016.
 */
public class FindPrimeNumbersAsyncTask extends AsyncTask<Double,Integer,List<Integer>>{

    private final ProgressDialog progressDialog;

    /**
     * WeakReference enable garbage collection of Activity.
     */
    WeakReference<MainActivity> mActivity;

    public FindPrimeNumbersAsyncTask(MainActivity activity){
        mActivity = new WeakReference<MainActivity>(activity);
        progressDialog = new ProgressDialog(mActivity.get());
        progressDialog.setCancelable(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                cancel(true);
            }
        });
    }

    @Override
    protected void onPreExecute() {
        progressDialog.show();
    }

    @Override
    protected List<Integer> doInBackground(Double... params) {


        Double number = params[0];
        List<Integer> factors = new ArrayList<Integer>();
        for (int i = 2; i <= number / i; i++) {
            while (number % i == 0) {
                publishProgress(i);
                factors.add(i);
                number /= i;
            }
        }
        if (number > 1) {
            factors.add(number.intValue());
        }

        try {
            Thread.sleep(1000);//this is just to show the progressbar
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        publishProgress(progressDialog.getMax());
        return factors;

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        Toast.makeText(mActivity.get(),mActivity.get().getResources().getString(R.string.operation_canceled),Toast.LENGTH_LONG).show();
        Log.d("Tag async","caneled async");
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progressDialog.setProgress(values[0]);
    }

    @Override
    protected void onPostExecute(List<Integer> integers) {
        if(progressDialog.isShowing())
            progressDialog.dismiss();

        mActivity.get().showResult(integers);
    }
}
